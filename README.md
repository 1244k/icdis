iCareDental Information System
==============================


1. There are 5 main entities/functions (sub-functions to be added soon):
    1. Patients
    2. Appointments
    3. Treatments
    4. Inventory
    5. Billing
    6. Branches


2. Patients functional requirements:
    1. Publish all patients
    2. Patients with all the branches profiles
    3. One patient specifically with its branch profile  


3. Appointments functional requirements:
    1. Publish all appointments by today/tomorrow/later
    2. Create appointment based on patient
    2. Appointments with all the patients profiles
    3. One appointment specifically with its patient profile


4. Treatments functional requirements:
    1. Publish all treatments by patients
    2. One patient specifically with its treatment, inventory and billing entry


5. Inventories functional requirements:
    1. Publish all inventory with all the suppliers profiles
    2. One Inventory with supplier

6. Billings functional requirements:
    1. Publish all billings total by day, week, month, custom
    2. One billing by each patient, inventory, treatment, supplier

7. Suppliers functional requirements:
    1. Publish all suppliers
    2. One supplier by inventory list
